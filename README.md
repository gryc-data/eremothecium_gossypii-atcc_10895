## *Eremothecium gossypii* ATCC 10895

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJNA13834](https://www.ebi.ac.uk/ena/browser/view/PRJNA13834)
* **Assembly accession**: [GCA_000091025.4](https://www.ebi.ac.uk/ena/browser/view/GCA_000091025.4)
* **Original submitter**: Biozentrum (University of Basel)

### Assembly overview

* **Assembly level**: Chromosome
* **Assembly name**: ASM9102v4
* **Assembly length**: 9,119,312
* **#Chromosomes**: 8
* **Mitochondiral**: Yes
* **N50 (L50)**: 1,519,140 (3)

### Annotation overview

* **Original annotator**: Biozentrum (University of Basel)
* **CDS count**: 4776
* **Pseudogene count**: 2
* **tRNA count**: 220
* **rRNA count**: 158
* **Mobile element count**: 0

# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.1 (2020-10-07)

## v1.0 (2020-10-07)

### Added

* The 9 annotated chromosomes of Eremothecium gossypii ATCC 10895 (source EBI, [GCA_000091025.4](https://www.ebi.ac.uk/ena/browser/view/GCA_000091025.4)).
